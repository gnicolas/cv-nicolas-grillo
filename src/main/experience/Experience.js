import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Grid from '@mui/material/Grid';
import Alert from '@mui/material/Alert';

import { Card, CardGroup} from "react-bootstrap";
import { Badge } from "react-bootstrap";

import { ICON_DONE } from '../../icons/Icon';
import { ICON_NOW } from '../../icons/Icon';
import {SVG_ANGULAR, SVG_BOOTSTRAP, SVG_INTELLIJ_IDEA, SVG_JAVASCRIPT, SVG_POSTMAN, SVG_SQL_DEVELOPER, SVG_TYPESCRIPT, SVG_VISUAL_STUDIO_CODE, SVG_JAVA} from "../../icons/Svg";
import {SVG_SWAGGER, SVG_JEST} from "../../icons/Svg";
import {motion} from 'framer-motion/dist/framer-motion';

import {getTimer} from "../../service/TimeService";

function createData(name, icon, colorIcon, time, back, front, software) {
    return { name, icon, colorIcon, time, back, front, software };
}

const mo2oJob = {
    title: "Mobile O2O, Developer",
    date: "Agosto 2022 - Actualidad"
}

const uluJob = {
    title: "Ulusoft, Developer",
    date: "Abril 2019 - Septiembre 2022"
}

const styleIconColorInfo = {backgroundColor: 'var(--color-info)'};
const styleIconColorOk = {backgroundColor: 'var(--color-ok)'};

function Experience(cc) {

    const mo2oAngularEndDate = new Date();
    const mo2oAngularStartDate = new Date(2022, 8, 1);
    const mo2oAngularTime = getTimer (mo2oAngularEndDate - mo2oAngularStartDate);

    const mo2oProyects = [
        createData('Angular Front End Developer', ICON_NOW, styleIconColorInfo, mo2oAngularTime,
            [SVG_ANGULAR, 'Angular', SVG_TYPESCRIPT, 'TypeScript'], [SVG_SWAGGER, 'Swagger API', SVG_JEST, 'Jest Unit Test'], [SVG_VISUAL_STUDIO_CODE, 'Visual Studio Code', SVG_POSTMAN, 'Postman'])
    ];

    const angularDeveloperEndDate = new Date();
    const angularDeveloperStartDate = new Date(2020, 8, 1);
    const angularDeveloperTime = getTimer (angularDeveloperEndDate - angularDeveloperStartDate);

    const javaDeveloperEndDate = new Date(2020, 7, 1);
    const javaDeveloperStartDate = new Date(2019, 4, 1);
    const javaDeveloperTime = getTimer(javaDeveloperEndDate - javaDeveloperStartDate);

    const uluProyects = [
        createData('Angular Full Stack Developer', ICON_DONE, styleIconColorOk, angularDeveloperTime,
            [SVG_ANGULAR, 'Angular', SVG_TYPESCRIPT, 'TypeScript'], [SVG_JAVA, 'Java', SVG_SQL_DEVELOPER, 'SQL'], [SVG_VISUAL_STUDIO_CODE, 'Visual Studio Code', SVG_INTELLIJ_IDEA, 'IntelliJ IDEA', SVG_POSTMAN, 'Postman']),
        /* createData('Node Backend Developer', ICON_DONE, styleIconColorOk, telecomTime,
            [SVG_NODE, 'Node', SVG_TYPESCRIPT, 'TypeScript', SVG_SQL_DEVELOPER, 'SQL'], [SVG_SWAGGER, 'Swagger API', SVG_JEST, 'Jest Unit Test'], [SVG_VISUAL_STUDIO_CODE, 'Visual Studio Code' , SVG_POSTMAN, 'Postman', SVG_SONAR, 'SonarQube']),
        */        
       //createData('Terminales del Río de la Plata S.A. - Angular Ionic Project ', ICON_DONE, styleIconColorOk, telecomTime, [SVG_TYPESCRIPT, 'TypeScript', SVG_HTML5, 'HTML5', SVG_CSS3, 'CSS3'], [SVG_ANGULAR, 'Angular', SVG_IONIC, 'Ionic'], [SVG_VISUAL_STUDIO_CODE, 'Visual Studio Code', SVG_SQL_DEVELOPER, 'Sql Developer']),
        createData('Java Developer', ICON_DONE, styleIconColorOk, javaDeveloperTime,
            [SVG_JAVA, 'Java', SVG_SQL_DEVELOPER, 'SQL'], [SVG_BOOTSTRAP, 'Bootstrap', SVG_JAVASCRIPT, 'JavaSprict'], [SVG_INTELLIJ_IDEA, 'IntelliJ IDEA', SVG_POSTMAN, 'Postman'])
    ];

    return (
        <div className="container">
            
            <motion.div
                initial={{ opacity: 0, scale: 0.8 }}
                whileInView={{ opacity: 1, scale: 1 }}
                viewport={{ once: false }}
                transition={{ duration: 0.5, ease: "easeOut" }}
            >

                <h3 className="font-weight-bold color-pri marginSection">Experiencia</h3>

                <Card style={{marginTop: 33}}>
                    <Card.Body>

                        <Grid container>
                            <Grid item xs>
                                <Typography>
                                    <h4>{mo2oJob.title}</h4>
                                </Typography>
                            </Grid>
                            <Grid item xs>
                                <Typography>
                                    <h4 className="text-right"><Badge bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-info)' ,marginLeft: '11px'}}>{mo2oJob.date}</Badge> </h4>
                                </Typography>
                            </Grid>
                        </Grid>

                        {mo2oProyects.map((row) => (
                            <Accordion style={{marginTop: '11px'}}>
                                <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">

                                    <div class="text-center" style={{width: 150, height: '100%'}}>
                                        <p style={{marginBottom: 6}}>{row.icon}</p>
                                        <Badge  bg="" className="btn-outline-light" style={row.colorIcon}>
                                            {row.time}
                                        </Badge>  
                                    </div>

                                    <Typography style={{width: '100%', paddingTop: 22}}>{row.name}</Typography>

                                    {/*
                                        <diV className="flex-testing-end" style={{width: '20%'}}>
                                            <p style={{marginBottom: 6}}>{ICON_ARROW_DOWN}</p>
                                        </diV>
                                    */}

                                </AccordionSummary>
                                <AccordionDetails>
                                    <Typography>

                                        <CardGroup>
                                            <Card>
                                                <Card.Body>
                                                <Card.Text >
                                                    <Alert style={{fontWeight: 'bold'}} severity='' color="info">
                                                        {row.back[0]}&nbsp;&nbsp;&nbsp;{row.back[1]}
                                                    </Alert>

                                                    {row.back[2] ? 
                                                        <Alert style={{marginTop: '22px', fontWeight: 'bold'}} severity='' color="info">
                                                            {row.back[2]}&nbsp;&nbsp;&nbsp;{row.back[3]}
                                                        </Alert> : null
                                                    }

                                                    {row.back[4] ? 
                                                        <Alert style={{marginTop: '22px', fontWeight: 'bold'}} severity='' color="info">
                                                            {row.back[4]}&nbsp;&nbsp;&nbsp;{row.back[5]}
                                                        </Alert> : null
                                                    }

                                                </Card.Text>
                                                </Card.Body>
                                            </Card>
                                            <Card>
                                                <Card.Body>
                                                <Card.Text>
                                                    <Alert style={{fontWeight: 'bold'}} severity='' color="info">
                                                        {row.front[0]}&nbsp;&nbsp;&nbsp;{row.front[1]}
                                                    </Alert>

                                                    {row.front[2] ? 
                                                        <Alert style={{marginTop: '22px', fontWeight: 'bold'}} severity='' color="info">
                                                            {row.front[2]}&nbsp;&nbsp;&nbsp;{row.front[3]}
                                                        </Alert> : null
                                                    }

                                                    {row.front[4] ? 
                                                        <Alert style={{marginTop: '22px', fontWeight: 'bold'}} severity='' color="info">
                                                            {row.front[4]}&nbsp;&nbsp;&nbsp;{row.front[5]}
                                                        </Alert> : null
                                                    }

                                                    
                                                </Card.Text>
                                                </Card.Body>
                                            </Card>
                                            <Card>
                                                <Card.Body>
                                                    <Alert style={{fontWeight: 'bold'}} severity='' color="info">
                                                        {row.software[0]}&nbsp;&nbsp;{row.software[1]}
                                                    </Alert>

                                                    {row.software[2] ? 
                                                        <Alert style={{marginTop: '22px', fontWeight: 'bold'}} severity='' color="info">
                                                            {row.software[2]}&nbsp;&nbsp;&nbsp;{row.software[3]}
                                                        </Alert> : null
                                                    }

                                                    {row.software[4] ? 
                                                        <Alert style={{marginTop: '22px', fontWeight: 'bold'}} severity='' color="info">
                                                            {row.software[4]}&nbsp;&nbsp;&nbsp;{row.software[5]}
                                                        </Alert> : null
                                                    }
                                                </Card.Body>
                                            </Card>
                                        </CardGroup>

                                    </Typography>
                                </AccordionDetails>
                            </Accordion>   
                        ))}
                    </Card.Body>
                </Card>

                <Card style={{marginTop: 33}}>
                    <Card.Body>

                        <Grid container>
                            <Grid item xs>
                                <Typography>
                                    <h4>{uluJob.title}</h4>
                                </Typography>
                            </Grid>
                            <Grid item xs>
                                <Typography>
                                    <h4 className="text-right"><Badge bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-ok)' ,marginLeft: '11px'}}>{uluJob.date}</Badge> </h4>
                                </Typography>
                            </Grid>
                        </Grid>

                        {uluProyects.map((row) => (
                            <Accordion style={{marginTop: '11px'}}>
                                <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">

                                    <div class="text-center" style={{width: 150, height: '100%'}}>
                                        <p style={{marginBottom: 6}}>{row.icon}</p>
                                        <Badge  bg="" className="btn-outline-light" style={row.colorIcon}>
                                            {row.time}
                                        </Badge>  
                                    </div>

                                    <Typography style={{width: '100%', paddingTop: 22}}>{row.name}</Typography>

                                    {/*
                                        <diV className="flex-testing-end" style={{width: '20%'}}>
                                            <p style={{marginBottom: 6}}>{ICON_ARROW_DOWN}</p>
                                        </diV>
                                    */}

                                </AccordionSummary>
                                <AccordionDetails>
                                    <Typography>

                                        <CardGroup>
                                            <Card>
                                                <Card.Body>
                                                <Card.Text >
                                                    <Alert style={{fontWeight: 'bold'}} severity='' color="info">
                                                        {row.back[0]}&nbsp;&nbsp;&nbsp;{row.back[1]}
                                                    </Alert>

                                                    {row.back[2] ? 
                                                        <Alert style={{marginTop: '22px', fontWeight: 'bold'}} severity='' color="info">
                                                            {row.back[2]}&nbsp;&nbsp;&nbsp;{row.back[3]}
                                                        </Alert> : null
                                                    }

                                                    {row.back[4] ? 
                                                        <Alert style={{marginTop: '22px', fontWeight: 'bold'}} severity='' color="info">
                                                            {row.back[4]}&nbsp;&nbsp;&nbsp;{row.back[5]}
                                                        </Alert> : null
                                                    }

                                                </Card.Text>
                                                </Card.Body>
                                            </Card>
                                            <Card>
                                                <Card.Body>
                                                <Card.Text>
                                                    <Alert style={{fontWeight: 'bold'}} severity='' color="info">
                                                        {row.front[0]}&nbsp;&nbsp;&nbsp;{row.front[1]}
                                                    </Alert>

                                                    {row.front[2] ? 
                                                        <Alert style={{marginTop: '22px', fontWeight: 'bold'}} severity='' color="info">
                                                            {row.front[2]}&nbsp;&nbsp;&nbsp;{row.front[3]}
                                                        </Alert> : null
                                                    }

                                                    {row.front[4] ? 
                                                        <Alert style={{marginTop: '22px', fontWeight: 'bold'}} severity='' color="info">
                                                            {row.front[4]}&nbsp;&nbsp;&nbsp;{row.front[5]}
                                                        </Alert> : null
                                                    }

                                                    
                                                </Card.Text>
                                                </Card.Body>
                                            </Card>
                                            <Card>
                                                <Card.Body>
                                                    <Alert style={{fontWeight: 'bold'}} severity='' color="info">
                                                        {row.software[0]}&nbsp;&nbsp;{row.software[1]}
                                                    </Alert>

                                                    {row.software[2] ? 
                                                        <Alert style={{marginTop: '22px', fontWeight: 'bold'}} severity='' color="info">
                                                            {row.software[2]}&nbsp;&nbsp;&nbsp;{row.software[3]}
                                                        </Alert> : null
                                                    }

                                                    {row.software[4] ? 
                                                        <Alert style={{marginTop: '22px', fontWeight: 'bold'}} severity='' color="info">
                                                            {row.software[4]}&nbsp;&nbsp;&nbsp;{row.software[5]}
                                                        </Alert> : null
                                                    }
                                                </Card.Body>
                                            </Card>
                                        </CardGroup>

                                    </Typography>
                                </AccordionDetails>
                            </Accordion>   
                        ))}
                    </Card.Body>
                </Card>

            </motion.div>

        </div>
    );
}   

export default Experience;
