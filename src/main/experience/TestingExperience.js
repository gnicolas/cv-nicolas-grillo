import * as React from 'react';
import { Badge } from "react-bootstrap";
import Chip from '@mui/material/Chip';

import { SVG_ULUSOFT } from "../../icons/Ulusoft";
import { SVG_NOW, SVG_DONE, SVG_SQL_DEVELOPER, SVG_NODE, SVG_JEST, SVG_BOOTSTRAP } from "../../icons/Svg";
import { SVG_ANGULAR, SVG_TYPESCRIPT, SVG_JAVA, SVG_JAVASCRIPT} from "../../icons/Svg";

import {getTimer} from "../../service/TimeService";


const MAX_HEIGHT = 150;

function TestingExperience(cc) {

    const terniumEndDate = new Date(); 
    const terniumStartDate = new Date(2022, 1, 1);

    const telecomEndDate = new Date(2022, 1, 1); 
    const telecomStartDate = new Date(2021, 4, 1);

    const trpEndDate = new Date(2021, 4, 1); 
    const trpStartDate = new Date(2020, 4, 1);

    const garbarinoEndDate = new Date(2020, 4, 1); 
    const garbarinoStartDate = new Date(2019, 4, 1);

    const terniumTime = getTimer( (terniumEndDate - terniumStartDate) + (trpEndDate - trpStartDate));
    const telecomTime = getTimer(telecomEndDate - telecomStartDate);
    // const trpTime = getTimer(trpEndDate - trpStartDate);
    const garbarinoTime = getTimer(garbarinoEndDate - garbarinoStartDate);


    return (
        <div className="container">
                
            <h3 className="font-weight-bold color-pri marginSection"> Experiencia </h3>

            {/* Ternium S.A. */}
            <div className='flex-testing' style={{marginTop: 22, backgroundColor: 'var()'}}>

                <div className='card-time-phone' style={{width: '100%'}}>
                    <div className="card-responsive " style={{padding: 11, width: '100%'}}>
                        <div className="card card-effect" style={{width: '100%'}}>
                            <div class="card-header" style={{height: '100%'}}>
                                <div className="flex-testing">
                                    <p>{SVG_NOW}
                                        <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-info)', marginTop: 4, fontSize: 11}}>
                                            {terniumTime}
                                        </Badge>
                                    </p>
                                    
                                    
                                    <Chip style={{marginTop: 11, fontSize: 13}} icon={SVG_ULUSOFT} label="Ulusoft" clickable />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='card-time-desktop'>
                    <div className='card-responsive' style={{padding: 11}}>
                        <div className="card card-effect" style={{width: 150}}>
                            <div class="card-header" style={{height: 200}}>
                                <div className="padre container" style={{height: 200}}>
                                    <div className="hijo text-center">
                                        <div className=" text-center">
                                            <p style={{marginBottom: 6}}>{SVG_NOW}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-info)', marginTop: 4, fontSize: 11}}>
                                                {terniumTime}
                                            </Badge>
                                            <p><Chip style={{marginTop: 11, fontSize: 13}} icon={SVG_ULUSOFT} label="Ulusoft" clickable /></p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div className="card-responsive card-body" style={{padding: 11}}>
                    <div className="card" style={{height: '100%'}}>

                        <div className="card-header" style={{height: 50}}>
                            <h5 style={{fontWeight: 'bold'}}> Angular Full Stack Developer </h5>
                        </div>

                        <div className='flex-testing'>
                            <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT}}>
                                <div className="card card-effect" >
                                    <div class="card-header">
                                        <div className=" text-center">
                                            <p>{SVG_ANGULAR}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-red)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                                Angular
                                            </Badge>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT}}>
                                <div className="card card-effect" >
                                    <div class="card-header">
                                        <div className=" text-center">
                                            <p>{SVG_TYPESCRIPT}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-blue)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                                TypeScript
                                            </Badge>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT}}>
                                <div className="card card-effect" >
                                    <div class="card-header">
                                        <div className=" text-center">
                                            <p>{SVG_JAVA}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-red)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                                Java
                                            </Badge>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT}}>
                                <div className="card card-effect" >
                                    <div class="card-header">
                                        <div className=" text-center">
                                            <p>{SVG_SQL_DEVELOPER}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-black)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                                SQL
                                            </Badge>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* Telecom S.A. */}
            <div className='flex-testing' style={{marginTop: 22, backgroundColor: 'var()'}}>

                <div className='card-time-phone' style={{width: '100%'}}>
                    <div className="card-responsive " style={{padding: 11, width: '100%'}}>
                        <div className="card card-effect" style={{width: '100%'}}>
                            <div class="card-header" style={{height: '100%'}}>
                                <div className="flex-testing">
                                    <p>{SVG_DONE}
                                        <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-ok)', marginTop: 4, fontSize: 11}}>
                                            {telecomTime}
                                        </Badge>
                                    </p>
                                    
                                    
                                    <Chip style={{marginTop: 11, fontSize: 13}} icon={SVG_ULUSOFT} label="Ulusoft" clickable />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='card-time-desktop'>
                    <div className='card-responsive' style={{padding: 11}}>
                        <div className="card card-effect" style={{width: 150}}>
                            <div class="card-header" style={{height: 200}}>
                                <div className="padre container" style={{height: 200}}>
                                    <div className="hijo text-center">
                                        <div className=" text-center">
                                            <p style={{marginBottom: 6}}>{SVG_DONE}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-ok)', marginTop: 4, fontSize: 11}}>
                                                {telecomTime}
                                            </Badge>
                                            <p><Chip style={{marginTop: 11, fontSize: 13}} icon={SVG_ULUSOFT} label="Ulusoft" clickable /></p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div className="card-responsive card-body" style={{padding: 11}}>
                    <div className="card" style={{height: '100%'}}>

                        <div className="card-header" style={{height: 50}}>
                            <h5 style={{fontWeight: 'bold'}}> Node Backend Developer </h5>
                        </div>

                        <div className='flex-testing'>
                            <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT}}>
                                <div className="card card-effect" >
                                    <div class="card-header">
                                        <div className=" text-center">
                                            <p>{SVG_NODE}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-ok)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                                Node
                                            </Badge>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT}}>
                                <div className="card card-effect" >
                                    <div class="card-header">
                                        <div className=" text-center">
                                            <p>{SVG_TYPESCRIPT}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-blue)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                                TypeScript
                                            </Badge>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT}}>
                                <div className="card card-effect" >
                                    <div class="card-header">
                                        <div className=" text-center">
                                            <p>{SVG_JEST}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-danger)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                                Jest
                                            </Badge>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT}}>
                                <div className="card card-effect" >
                                    <div class="card-header">
                                        <div className=" text-center">
                                            <p>{SVG_SQL_DEVELOPER}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-black)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                                SQL
                                            </Badge>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* Garbarino S.A. */}
            <div className='flex-testing' style={{marginTop: 22, backgroundColor: 'var()'}}>

                <div className='card-time-phone' style={{width: '100%'}}>
                    <div className="card-responsive " style={{padding: 11, width: '100%'}}>
                        <div className="card card-effect" style={{width: '100%'}}>
                            <div class="card-header" style={{height: '100%'}}>
                                <div className="flex-testing">
                                    <p>{SVG_DONE}
                                        <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-ok)', marginTop: 4, fontSize: 11}}>
                                            {garbarinoTime}
                                        </Badge>
                                    </p>
                                    
                                    <Chip style={{marginTop: 11, fontSize: 13}} icon={SVG_ULUSOFT} label="Ulusoft" clickable />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='card-time-desktop'>
                    <div className='card-responsive' style={{padding: 11}}>
                        <div className="card card-effect" style={{width: 150}}>
                            <div class="card-header" style={{height: 200}}>
                                <div className="padre container" style={{height: 200}}>
                                    <div className="hijo text-center">
                                        <div className=" text-center">
                                            <p style={{marginBottom: 6}}>{SVG_DONE}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-ok)', marginTop: 4, fontSize: 11}}>
                                                {garbarinoTime}
                                            </Badge>
                                            <p><Chip style={{marginTop: 11, fontSize: 13}} icon={SVG_ULUSOFT} label="Ulusoft" clickable /></p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div className="card-responsive card-body" style={{padding: 11}}>
                    <div className="card" style={{height: '100%'}}>

                        <div className="card-header" style={{height: 50}}>
                            <h5 style={{fontWeight: 'bold'}}> Java Developer </h5>
                        </div>

                        <div className='flex-testing'>
                            <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT}}>
                                <div className="card card-effect" >
                                    <div class="card-header">
                                        <div className=" text-center">
                                            <p>{SVG_JAVA}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-red)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                                Java
                                            </Badge>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT}}>
                                <div className="card card-effect" >
                                    <div class="card-header">
                                        <div className=" text-center">
                                            <p>{SVG_BOOTSTRAP}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-danger)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                                Bootstrap
                                            </Badge>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT}}>
                                <div className="card card-effect" >
                                    <div class="card-header">
                                        <div className=" text-center">
                                            <p>{SVG_JAVASCRIPT}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-yellow)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                                JavaSprict
                                            </Badge>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT}}>
                                <div className="card card-effect" >
                                    <div class="card-header">
                                        <div className=" text-center">
                                            <p>{SVG_SQL_DEVELOPER}</p>
                                            <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-black)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                                SQL
                                            </Badge>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
}
  
export default TestingExperience;