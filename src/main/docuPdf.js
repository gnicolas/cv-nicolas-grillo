import { Document, Text, Page, View } from "@react-pdf/renderer"

// const imgSize = "196px";
// const bgBtnHeader = 'var(--color-header)';

const FULLNAME = process.env.REACT_APP_FULLNAME;
const TITLE = 'Desarrollador Full Stack'
const PHONE = process.env.REACT_APP_PHONE;
// const EMAIL = process.env.REACT_APP_EMAIL;
// const BIRTH = '09/04/1997';

const svgPhone = 
<svg width="25" height="25" viewBox="0 0 142 142" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M121.41 20.4066C107.92 7.36625 90.17 0 71.2663 0C17.0104 0 -17.0577 58.7821 10.0347 105.541L0 142L37.4821 132.226C53.8121 141.047 67.1897 140.266 71.3017 140.787C134.214 140.787 165.531 64.6751 121.321 20.6373L121.41 20.4066Z" fill="white"/>
    <path d="M71.3968 128.694L71.3613 128.688H71.2666C52.4398 128.688 40.4112 119.771 39.2279 119.257L17.0404 125.025L22.9866 103.459L21.5726 101.24C15.7151 91.9156 12.6029 81.1768 12.6029 70.1186C12.6029 18.0933 76.1775 -7.92225 112.967 28.8498C149.668 65.2373 123.901 128.694 71.3968 128.694Z" fill="#333333"/>
    <path d="M103.583 84.6493L103.53 85.0931C101.749 84.2056 93.0748 79.9633 91.4596 79.3776C87.8327 78.0345 88.8562 79.1646 81.8923 87.1402C80.8569 88.294 79.8274 88.3827 78.0701 87.584C76.2951 86.6965 70.5974 84.8327 63.8524 78.7977C58.5984 74.094 55.0721 68.3252 54.0307 66.5502C52.2971 63.5564 55.9241 63.1304 59.2256 56.8824C59.8172 55.6399 59.5155 54.6636 59.0776 53.7821C58.6339 52.8946 55.1016 44.1971 53.6225 40.7299C52.2025 37.2746 50.7411 37.7124 49.6465 37.7124C46.2385 37.4166 43.7476 37.4639 41.5525 39.7477C32.003 50.2439 34.4111 61.0714 42.582 72.5852C58.6398 93.6012 67.1953 97.4707 82.839 102.843C87.0635 104.186 90.9152 103.997 93.9623 103.559C97.3585 103.021 104.417 99.2931 105.89 95.1218C107.399 90.9506 107.399 87.4893 106.955 86.6906C106.517 85.8918 105.358 85.4481 103.583 84.6493Z" fill="#FAFAFA"/>
</svg>;

function DocuPdf(cc) {

    return (
        <Document >
            <Page style={{display: "flex", flexDirection: "row", justifyContent: "center", alignItems: "center"}} size={'A4'} >
                <View  style={{ backgroundColor: "#2471A3", width: '31%', height: '100%'}}>
                    <Text style={{color: "#ffffff"}}></Text>
                </View>
                <View style={{ backgroundColor: "", width: '69%', height: '100%'}}>
                    <Text style={{color: "#000"}}>{FULLNAME}</Text>
                    <Text style={{color: "#000"}}>{TITLE}</Text>
                    <Text href="#" underline="hover" style={{color: "#000"}}>https://cvnicolasgrillo.web.app</Text>


                    <Text style={{color: "#000"}}>{svgPhone}{PHONE}</Text>
                </View>
            </Page>
        </Document>
    );
}
  
export default DocuPdf;