import { Table } from "react-bootstrap";
import { Badge } from "react-bootstrap";
import { Card, CardGroup} from "react-bootstrap";

import Avatar from '@mui/material/Avatar';
import Chip from '@mui/material/Chip';
import argentine from "./argentine.png";

import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import Alert from '@mui/material/Alert';

import { ICON_BOOK, ICON_DONE, ICON_NOW } from "../../icons/Icon";

import {motion} from 'framer-motion/dist/framer-motion';

function createData(name, date, details, icon, color) {
    return { name, date, details, icon, color };
}

const proyects = [
    createData('Ingeniería en Informática ', "En Curso", 'Universidad Nacional de La Matanza | UNLaM', ICON_NOW, 'var(--color-info)'),
    createData('Diplomatura Full Stack Developer ', "2020", 'Universidad Tecnológica Nacional | UTN', ICON_DONE, 'var(--color-ok)'),
    createData('Professional Front End Developer ', "2019", 'Universidad Tecnológica Nacional | UTN', ICON_DONE, 'var(--color-ok)'),
    createData('Administración ', "2014", 'Instituto Ntra. Sra. Del Buen y Perpetuo Socorro', ICON_DONE, 'var(--color-ok)')
];

function Education(cc) {

    return (
        <div className="container">

            <motion.div
                initial={{ opacity: 0, scale: 0.8 }}
                whileInView={{ opacity: 1, scale: 1 }}
                viewport={{ once: false }}
                transition={{ duration: 0.5, ease: "easeOut" }}
            >
                <h3 className="font-weight-bold color-pri marginSection">Educación</h3>
                
                <Table responsive="md">
                    <tbody>
                    {proyects.map((row) => (
                    <tr>
                        <td colSpan={2}>

                        <Accordion>
                            <AccordionSummary aria-controls="panel1a-content" id="panel1a-header">
                                {row.icon}
                                <Typography>&nbsp;&nbsp;{row.name} <Badge bg="" className="btn-outline-light" style={{backgroundColor: row.color ,marginLeft: '11px'}}>{row.date}</Badge>  </Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Typography>
                                    <Chip style={{marginBottom: 5, fontSize: 15}} avatar={<Avatar alt="es" src={argentine} />} label={row.details} />
                                </Typography>
                            </AccordionDetails>
                        </Accordion>

                        </td>
                    </tr>           
                    ))}
                    </tbody>
                </Table>

            </motion.div>

            <motion.div
                initial={{ opacity: 0, scale: 0.8 }}
                whileInView={{ opacity: 1, scale: 1 }}
                viewport={{ once: false }}
                transition={{ duration: 0.5, ease: "easeOut" }}
            >

                <h3 className="font-weight-bold color-pri marginSection">Idioma</h3>

                <Table responsive="md">
                    <tbody>
                    <tr>
                        <td colSpan={2}>
                        <Accordion style={{marginTop: '11px'}}>
                            <AccordionSummary aria-controls="panel1a-content" id="panel1a-header">
                                {ICON_BOOK}
                                <Typography>&nbsp;&nbsp; English &nbsp;<Badge bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-danger)', marginLeft: '11px'}}>Avanzado</Badge></Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                            <Typography>
                                <CardGroup>
                                    <Card>
                                        <Card.Body>
                                        <Card.Title style={{textDecoration: ''}}>Speaking</Card.Title>
                                        <Card.Text >
                                            <Alert style={{fontWeight: 'bold'}} severity='' color="info">
                                                Intermediate
                                            </Alert>
                                        </Card.Text>
                                        </Card.Body>
                                    </Card>
                                    <Card>
                                        <Card.Body>
                                        <Card.Title style={{textDecoration: ''}}>Writing</Card.Title>
                                        <Card.Text>
                                            <Alert style={{fontWeight: 'bold'}} severity='' color="secondary">
                                                Advanced
                                            </Alert>
                                        </Card.Text>
                                        </Card.Body>
                                    </Card>
                                    <Card>
                                        <Card.Body>
                                        <Card.Title style={{textDecoration: ''}}>Listening</Card.Title>
                                            <Alert style={{fontWeight: 'bold'}} severity='' color="secondary">
                                                Advanced
                                            </Alert>
                                        </Card.Body>
                                    </Card>
                                </CardGroup>
                                </Typography>
                            </AccordionDetails>
                        </Accordion>

                        </td>
                    </tr>
                    </tbody>
                </Table>


            </motion.div>

        </div>
    );
}   

export default Education;