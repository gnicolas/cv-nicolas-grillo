
function SvgComponent(component) {

    return (
        <div>
            {component.svg}
        </div>
    );
}   
  
export default SvgComponent;