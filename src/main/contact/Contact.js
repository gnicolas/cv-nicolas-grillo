
import Avatar from '@mui/material/Avatar';
import image from "./nicolas.jpg";
import spain from "./spain.png";

import { Badge } from "react-bootstrap";

import Chip from '@mui/material/Chip';

import { SVG_ADDRESS, SVG_BIRTH, SVG_EMAIL, SVG_LINKEDING, SVG_NOW } from "../../icons/Svg";

import { motion } from 'framer-motion/dist/framer-motion';
import { getTimer } from "../../service/TimeService";

import { SVG_O2O } from '../../icons/O2O';

const imgSize = "196px";

const FULLNAME = process.env.REACT_APP_FULLNAME;
const TITLE = process.env.REACT_APP_TITLE;
//const PHONE = (process.env.REACT_APP_PHONE === "null") ? null : process.env.REACT_APP_PHONE;
const EMAIL = process.env.REACT_APP_EMAIL;
const LINKEDIN = process.env.REACT_APP_LINKEDIN;
const ADDRESS = (process.env.REACT_APP_ADDRESS === "null") ? null : process.env.REACT_APP_ADDRESS;
const BIRTH = '09/04/1997';

function Contact(cc) {

    const MAX_HEIGHT = 219;//181;

    const today = new Date();
    const o2oStartDate = new Date(2022, 8, 1);

    const o2oDate = getTimer(today - o2oStartDate);

    return (
        <div className="container">

            <motion.div
                initial={{ opacity: 0, scale: 0.8 }}
                whileInView={{ opacity: 1, scale: 1 }}
                viewport={{ once: false }}
                transition={{ duration: 0.5, ease: "easeOut" }}
            >

                <h1 id='fullname' className="font-weight-bold color-pri">{FULLNAME}</h1>
                <h3 id='title' className="text-secondary" >{TITLE}</h3>

                <div id="phone-avatar">
                    <Avatar style={{ width: imgSize, height: imgSize, margin: 'auto' }} alt="x" src={image} />
                </div>

                {/* 
                    variant="outlined" 
                    <Chip avatar={<Avatar alt="es" src={spain} />} label="Madrid"/>
                    <Chip avatar={<Avatar alt="es" src={argentine} />} label="Buenos Aires"/>
                */}

                <div className='flex-testing'>

                    <div className="card-body card-responsive">
                        <div className="card" style={{ height: MAX_HEIGHT }}>
                            <div class="card-body" >

                                {/* <h5> &nbsp; {svgPhone} &nbsp; {PHONE}</h5> */}

                                <h5>{SVG_EMAIL}&nbsp;{EMAIL}</h5>
                                <a href={LINKEDIN} style={{ color: 'var(--color-text)', textDecoration: 'none' }}>
                                    <h5>{SVG_LINKEDING}&nbsp;www.linkedin.com/in/nicolas-grillo</h5>
                                </a>
                                {ADDRESS ? <h5>{SVG_ADDRESS}&nbsp;{ADDRESS}</h5> : null}
                                <h5>{SVG_BIRTH}&nbsp;{BIRTH}</h5>

                                <Chip style={{ marginTop: 11 }} avatar={<Avatar alt="es" src={spain} />} label="Madrid" />

                            </div>
                        </div>
                    </div>

                    <div className="card-body card-responsive text-center">
                        <a id="link" href="#experience">
                            <div className="card card-effect">
                                <div class="card-header" style={{ height: MAX_HEIGHT }}>
                                    <div className=" text-center">
                                        <p style={{ marginTop: 15, marginBottom: 5 }}>{SVG_NOW}</p>
                                        <Badge bg="" className="btn-outline-light" style={{ backgroundColor: 'var(--color-info)', fontSize: 11 }}>
                                            {o2oDate}
                                        </Badge>
                                        <p>
                                            <Chip style={{ marginTop: 14, fontSize: 13 }} icon={SVG_O2O} label="Mobile One2One" />
                                        </p>

                                        <Chip avatar={<Avatar alt="es" src={spain} />} label="Madrid" />
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                    {/* <div className="card-body card-responsive text-center">
                        <a id="link" href="#experience">
                            <div className="card card-effect">
                                <div class="card-header" style={{height: MAX_HEIGHT}}>
                                    <div className=" text-center">
                                        <p style={{marginTop: 15, marginBottom: 5}}>{SVG_NOW}</p>
                                        <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-info)', fontSize: 11}}>
                                            {ulusoftDate}
                                        </Badge>
                                        <p>
                                            <Chip style={{marginTop: 14, fontSize: 13}} icon={SVG_ULUSOFT} label="Ulusoft" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div> */}

                </div>

            </motion.div>

        </div>
    );
}

export default Contact;