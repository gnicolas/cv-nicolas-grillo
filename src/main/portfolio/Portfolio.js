import * as React from 'react';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';

import CyClaves from './CyClaves.png'
import ApiIonic from './api-ionic.png'
import CVNicolasGrillo from './webcv.png'

import { ICON_JAVASCRIPT, ICON_REACT } from '../../icons/Icon';
import {SVG_ANDRIOD, SVG_GIT, SVG_IONIC, SVG_LINK} from "../../icons/Svg";

import {motion} from 'framer-motion/dist/framer-motion';

function Portfolio(cc) {

    return (
        <div className="container">

            <motion.div
                initial={{ opacity: 0, scale: 0.8 }}
                whileInView={{ opacity: 1, scale: 1 }}
                viewport={{ once: false }}
                transition={{ duration: 0.5, ease: "easeOut" }}
            >

                <h3 className="font-weight-bold color-pri marginSection"> Portafolio </h3>

                <div className="row">  
                    <div className="col-sm-4" style={{marginTop: '11px', marginBottom: '11px'}}>
                        <div className="card card-effect" >
                            <div class="card-header">
                                {ICON_REACT}&nbsp;{SVG_IONIC}
                            </div>
                            <CardMedia id="img-api-ionic"
                                component="img"
                                alt="green iguana"
                                height="140"
                                image={ApiIonic}
                            />
                            <div className="card-body card-responsive">
                                <h4 class="card-text">React Ionic</h4>
                                <h5 class="card-title">Aplicación React con Ionic, hace consultas Api Rest con Axios.</h5>
                                <Button style={{marginTop: '22px'}} href="https://gitlab.com/gnicolas/api-ionic" size="small">
                                    {SVG_GIT}&nbsp;Repositorio Git
                                </Button>
                            </div>
                        </div>
                    </div>

                    <div className="col-sm-4" style={{marginTop: '11px', marginBottom: '11px'}}>
                        <div className="card card-effect" >
                            <div class="card-header">
                                {SVG_ANDRIOD}
                            </div>
                            <CardMedia id="img-cy-claves"
                                component="img"
                                alt="green iguana"
                                height="140"
                                image={CyClaves}
                            />
                            <div className="card-body card-responsive">
                                <h4 class="card-text">Android</h4>
                                <h5 class="card-title">Aplicación nativa en Android Studio en Java, publicada en la Play Store.</h5>
                                <Button style={{marginTop: '22px'}} href="https://play.google.com/store/apps/details?id=com.mistresapps.grill.cy" size="small">
                                    {SVG_LINK}&nbsp;ver
                                </Button>
                            </div>
                        </div>
                    </div>

                    <div className="col-sm-4" style={{marginTop: '11px', marginBottom: '11px'}}>
                        <div className="card card-effect" >
                            <div class="card-header">
                                {ICON_REACT}&nbsp;{ICON_JAVASCRIPT}
                            </div>
                            <CardMedia id="img-cv-nicolas-grillo"
                                component="img"
                                alt="green iguana"
                                height="140"
                                image={CVNicolasGrillo}
                            />
                            <div className="card-body card-responsive" >
                                <h4 class="card-text"> React Js.</h4>

                                <h5 class="card-title"> Currículum Vítae Web, desarrollado en React.</h5>
                                <Button style={{marginTop: '22px'}} href="https://gitlab.com/gnicolas/cv-nicolas-grillo" size="small">
                                    {SVG_GIT}&nbsp;Repositorio Git
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
                
            </motion.div>

           
            
        </div>
    );
}   

export default Portfolio;