import { Table } from "react-bootstrap";
import { Badge } from "react-bootstrap";

import { ICON_DONE, ICON_NOW } from "../../icons/Icon";

import {motion} from 'framer-motion/dist/framer-motion';

function createData(name, date, details, icon, color) {
    return { name, date, details, icon, color };
}
  
const proyects = [
    createData('Complete React Course - Learn From Scratch', "Udemy, En Curso", null, ICON_NOW, 'var(--color-info)'),
    createData('Angular & NodeJS – The Mean Stack Guide (2021)', "Udemy, 2021", null, ICON_DONE, 'var(--color-ok)'),
    createData('Ionic 6+: Crear Aplicaciones IOS, Android y PWA con Angular', "Udemy, 2021", null, ICON_DONE, 'var(--color-ok)'),
    createData('Programación en Base de Datos Oracle - Lenguaje PL/SQL', "UTN, 2020", null, ICON_DONE, 'var(--color-ok)'),
    createData('Diseño en experiencia de usuario e interacciones UX/UI ', "EducacionIT, 2020", null, ICON_DONE, 'var(--color-ok)'),
    createData('Desarrollo Web en Bootstrap', "UTN, 2019", null, ICON_DONE, 'var(--color-ok)'),
    createData('Desarrollo Web con JavaScript', "UTN, 2019", null, ICON_DONE, 'var(--color-ok)'),
    createData('Desarrollo Web en Html5 y Css3', "UTN, 2019", null, ICON_DONE, 'var(--color-ok)')
];

function Courses(cc) {

    return (
        <motion.div className="container"
            initial={{ opacity: 0, scale: 0.8 }}
            whileInView={{ opacity: 1, scale: 1 }}
            viewport={{ once: false }}
            transition={{ duration: 0.5, ease: "easeOut" }}
        >

            <h3 className="font-weight-bold color-pri marginSection">Cursos</h3>
            
            <Table responsive="md">
                <tbody>
                    {proyects.map((row) => (
                        <tr>
                            <td><h4>{row.icon}&nbsp;&nbsp;{row.name}  </h4>  </td> 
                            <td className="text-right"><h4><Badge bg="" className="btn-outline-light" style={{backgroundColor: row.color ,marginLeft: '11px'}}>{row.date}</Badge> </h4></td>
                        </tr>
                    ))}
                </tbody>
            </Table>

        </motion.div>
    );
}   

export default Courses;