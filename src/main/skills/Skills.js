import { Card } from "react-bootstrap";
import Chip from '@mui/material/Chip';

import {motion} from 'framer-motion/dist/framer-motion';

function Skills(cc) {

    return (
        <div className="container">

            <motion.div
                initial={{ opacity: 0, scale: 0.8 }}
                whileInView={{ opacity: 1, scale: 1 }}
                viewport={{ once: false }}
                transition={{ duration: 0.5, ease: "easeOut" }}
            >

                <h3 className="font-weight-bold color-pri marginSection">Conocimientos</h3>

                <Card style={{marginTop: '33px'}}>
                    <Card.Body>
                    <h4 className="font-weight-bold color-pri">Lenguaje de Programación:</h4>
                        <div style={{marginTop: 20 }}>
                            <Chip style={{margin: 5, padding: 5, fontSize: 14}} icon="" label="JAVA" />
                            <Chip style={{margin: 5, padding: 5, fontSize: 14}} icon="" label="HTML5" />
                            <Chip style={{margin: 5, padding: 5, fontSize: 14}} icon="" label="CSS3" />
                            <Chip style={{margin: 5, padding: 5, fontSize: 14}} icon="" label="TypeScript" />
                            <Chip style={{margin: 5, padding: 5, fontSize: 14}} icon="" label="JavaScript" />
                            <Chip style={{margin: 5, padding: 5, fontSize: 14}} icon="" label="C/C++" />
                        </div>
                    </Card.Body>
                </Card>

                <Card style={{marginTop: '33px'}}>
                    <Card.Body>
                    <h4 className="font-weight-bold color-pri">Frameworks:</h4>
                        <div style={{marginTop: 20 }}>
                            <Chip style={{margin: 5, padding: 5, fontSize: 14}} icon="" label="Angular" />
                            <Chip style={{margin: 5, padding: 5, fontSize: 14}} icon="" label="Node.js" />
                            <Chip style={{margin: 5, padding: 5, fontSize: 14}} icon="" label="React" />
                        </div>
                        <div>
                            <Chip style={{margin: 5, padding: 5, fontSize: 14}} icon="" label="Ionic" />
                            <Chip style={{margin: 5, padding: 5, fontSize: 14}} icon="" label="Bootstrap" />
                            <Chip style={{margin: 5, padding: 5, fontSize: 14}} icon="" label="Swagger (API Documentación)" />
                        </div>
                    </Card.Body>
                </Card>

                <Card style={{marginTop: '33px'}}>
                    <Card.Body>
                    <h4 className="font-weight-bold color-pri">Conocimientos IT:</h4>
                        <Card.Text>
                            <h4>Linux y Git</h4>
                            <h4>Experiencia en gestión de compilaciones y deploys de pipelines CI / CD en GitHub, GitLab y Jenkins.</h4>
                            <h4>Desarrollo de pruebas unitarias Unit Test, Jest y SonarQube.</h4>
                            <h4>Conocimientos en tecnologías de containers (Kubernete2s).</h4>
                            <h4>Experiencia en el desarrollo de servicios restful webs utilizando Web API.</h4>
                            <h4>Experiencia con bibliotecas (frameworks) web modernas como Angular.</h4>
                            <h4>Refactor de código para mejorar la eficiencia y mantenibilidad.</h4>
                            <h4>Mejora continua de conocimientos y formación.</h4>
                        </Card.Text>
                    </Card.Body>
                </Card>

                <Card style={{marginTop: '33px'}}>
                    <Card.Body>
                        <h4 className="font-weight-bold color-pri">Experiencia trabajando en equipo:</h4>
                        <Card.Text>
                            <h4>Metodología Scrum, experiencia trabajando en equipos ágiles.</h4>
                            <h4>Teamcity - Jira - Confluence - GitHub - GitLab - Teams</h4>
                        </Card.Text>
                    </Card.Body>
                </Card>

                <Card style={{marginTop: '33px'}}>
                    <Card.Body>
                    <h4 className="font-weight-bold color-pri">Conocimientos en Programación Web:</h4>
                        <Card.Text>
                            <h4>Experiencia laboral en desarrollos web Typescript, JavaScript, HTM5 y CSS3.</h4>
                            <h4>Utilización de distintos frameworks para el desarrollo front-end web.</h4>              
                        </Card.Text>
                    </Card.Body>
                </Card>
                    
                <Card style={{marginTop: '33px'}}>
                    <Card.Body>
                    <h4 className="font-weight-bold color-pri">Conocimientos en Aplicaciones Móviles:</h4>
                        <Card.Text>
                            <h4>Experiencia desarrollando aplicaciones nativas Android usando Android Studio, programando en Java y gestion en la Play Store.</h4>
                        </Card.Text>
                    </Card.Body>
                </Card>

                <Card style={{marginTop: '33px'}}>
                    <Card.Body>
                    <h4 className="font-weight-bold color-pri">Conocimientos en Desarrollo Backend:</h4>
                        <Card.Text>
                            <h4>Desarrollo Backend, JAVA y Typescript</h4>
                            <h4>Desarrollos de API Rest.</h4>
                        </Card.Text>
                    </Card.Body>
                </Card>

                <Card style={{marginTop: '33px'}}>
                    <Card.Body>
                    <h4 className="font-weight-bold color-pri">Base de Datos:</h4>
                        <Card.Text>
                        <h4>Experiencia trabajando con bases de datos SQL y NoSQL</h4>
                        <h4>SQL Developer, MYSQL, Firebase, Azure Data Studio</h4>
                        </Card.Text>
                    </Card.Body>
                </Card>

            </motion.div>

        </div>
    );
}   

export default Skills;