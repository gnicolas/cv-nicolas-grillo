import * as React from 'react';
import { Badge } from "react-bootstrap";
import { ICON_REACT, ICON_SPRING_BOOT} from "../../icons/Icon";
import { SVG_ANGULAR, SVG_CSS3, SVG_HTML5, SVG_JAVA, SVG_JAVASCRIPT, SVG_NODE, SVG_SQL_DEVELOPER, SVG_TYPESCRIPT } from '../../icons/Svg';
// import { SVG_JAVA, SVG_JAVASCRIPT, SVG_SQL_DEVELOPER, SVG_TYPESCRIPT } from '../../icons/Svg';

import {getTimer} from "../../service/TimeService";

const MAX_HEIGHT = 140;
const MARGIN_TOP_CARD = 33;

function Statistics(cc) {
    const today = new Date();
    const ulusoftStartDate = new Date(2019, 4, 1);

    const terniumEndDate = new Date(); 
    const terniumStartDate = new Date(2022, 1, 1);

    const telecomEndDate = new Date(2022, 1, 1); 
    const telecomStartDate = new Date(2021, 4, 1);

    const trpEndDate = new Date(2021, 4, 1); 
    const trpStartDate = new Date(2020, 4, 1);

    const garbarinoEndDate = new Date(2020, 4, 1); 
    const garbarinoStartDate = new Date(2019, 4, 1);

    //const htmlTime = getTimer(today - ulusoftStartDate);
    const javaYears = getTimer( (terniumEndDate - terniumStartDate) + (trpEndDate - trpStartDate) + (garbarinoEndDate - garbarinoStartDate));
    const typescriptYears = getTimer( (terniumEndDate - terniumStartDate) + (telecomEndDate - telecomStartDate) + (trpEndDate - trpStartDate));
    //const javascriptYears = getTimer((garbarinoEndDate - garbarinoStartDate));

    const sqlTime = getTimer(today - ulusoftStartDate);
    
    //const angularTime = getTimer((terniumEndDate - terniumStartDate) + (trpEndDate - trpStartDate));
    const nodeTime = getTimer(telecomEndDate - telecomStartDate);

    return (
        <div className="container marginBottomSection">
                
            <h3 className="font-weight-bold color-pri marginTopSection"> Frameworks </h3>

            <div className='flex-testing marginBottomSection'>
                <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT, marginTop: MARGIN_TOP_CARD}}>
                    <div className="card card-effect">
                        <div class="card-header">
                            <div className=" text-center">
                                <p>
                                    <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-ok)', marginTop: 4, fontSize: 11}}>
                                        Spring Boot
                                    </Badge>
                                </p>
                                <p>{ICON_SPRING_BOOT}</p>
                                <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-ok)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                    {javaYears}
                                </Badge>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT, marginTop: MARGIN_TOP_CARD}}>
                    <div className="card card-effect">
                        <div class="card-header" >
                            <div className=" text-center">
                                <p>
                                    <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-red)', marginTop: 4, fontSize: 11}}>
                                        Angular
                                    </Badge>
                                </p>
                                <p>{SVG_ANGULAR}</p>
                                <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-red)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                    2 años, 9 meses
                                </Badge>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT, marginTop: MARGIN_TOP_CARD}}>
                    <div className="card card-effect">
                        <div class="card-header" >
                            <div className=" text-center">
                                <p>
                                    <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-info)', marginTop: 4, fontSize: 11}}>
                                        React
                                    </Badge>
                                </p>
                                <p>{ICON_REACT}</p>
                                <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-info)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                    1 año
                                </Badge>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT, marginTop: MARGIN_TOP_CARD}}>
                    <div className="card card-effect">
                        <div class="card-header">
                            <div className=" text-center">
                                <p>
                                    <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-ok)', marginTop: 4, fontSize: 11}}>
                                        Node
                                    </Badge>
                                </p>
                                <p>{SVG_NODE}</p>
                                <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-ok)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                    {nodeTime}
                                </Badge>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <h3 className="font-weight-bold color-pri marginTopSection"> Lenguajes de Programación </h3>

            <div className='flex-testing'>
                <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT, marginTop: MARGIN_TOP_CARD}}>
                    <div className="card card-effect" >
                        <div class="card-header">
                            <div className=" text-center">
                                <p>
                                    <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-danger)', marginTop: 4, fontSize: 11}}>
                                        HTML y CSS
                                    </Badge>
                                </p>
                                <p>{SVG_HTML5} {SVG_CSS3}</p>
                                <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-danger)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                    {sqlTime}
                                </Badge>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT, marginTop: MARGIN_TOP_CARD}}>
                    <div className="card card-effect" >
                        <div class="card-header">
                            <div className=" text-center">
                                <p>
                                    <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-black)', marginTop: 4, fontSize: 11}}>
                                        SQL
                                    </Badge>
                                </p>
                                <p>{SVG_SQL_DEVELOPER}</p>
                                <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-black)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                    {sqlTime}
                                </Badge>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT, marginTop: MARGIN_TOP_CARD}}>
                    <div className="card card-effect" >
                        <div class="card-header">
                            <div className=" text-center">
                                <p>
                                    <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-orange)', marginTop: 4, fontSize: 11}}>
                                        Java
                                    </Badge>
                                </p>
                                <p>{SVG_JAVA}</p>
                                <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-orange)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                    {javaYears}
                                </Badge>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT, marginTop: MARGIN_TOP_CARD}}>
                    <div className="card card-effect" >
                        <div class="card-header">
                            <div className=" text-center">
                                <p>
                                    <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-blue)', marginTop: 4, fontSize: 11}}>
                                        TypeScript
                                    </Badge>
                                </p>
                                <p>{SVG_TYPESCRIPT}</p>
                                <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-blue)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                    {typescriptYears}
                                </Badge>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-body card-responsive text-center" style={{height: MAX_HEIGHT, marginTop: MARGIN_TOP_CARD}}>
                    <div className="card card-effect" >
                        <div class="card-header">
                            <div className=" text-center">
                                <p>
                                    <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-yellow)', marginTop: 4, fontSize: 11}}>
                                        JavaSprict
                                    </Badge>
                                </p>
                                <p>{SVG_JAVASCRIPT}</p>
                                <Badge  bg="" className="btn-outline-light" style={{backgroundColor: 'var(--color-yellow)', marginTop: 4, fontSize: 11, width: '100%'}}>
                                    1 año
                                </Badge>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
}
  
export default Statistics;