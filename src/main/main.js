import { Component } from "react";
import './main.css';
import Contact from "./contact/Contact";
import Experience from "./experience/Experience";
import Education from "./education/Education";
import Courses from "./courses/Courses";
import Skills from "./skills/Skills";
import Portfolio from "./portfolio/Portfolio";
import Statistics from "./statistics/Statistics";
//import Testing from "./testing/Testing";
//import TestingExperience from "./experience/TestingExperience";

class Main extends Component {

    render() {
      return (
        <div id="main" className="marginPage">

            <section id="contact">
              <Contact></Contact>
            </section>

            <section id="statistics">
              <Statistics></Statistics>
            </section>

            <section id="education">
              <Education></Education>
            </section>
            
            {/*
              <section id="testing">
                <Testing></Testing>
              </section>
            */}

            <section id="experience">
              <Experience></Experience>
              {/* <TestingExperience></TestingExperience> */}
            </section>

            <section id="portfolio">
              <Portfolio></Portfolio>
            </section>

            <section id="courses">
              <Courses></Courses>
            </section>

            <section id="additional">
              <Skills></Skills>
            </section>

        </div>
      );
    }
  }

  export default Main;