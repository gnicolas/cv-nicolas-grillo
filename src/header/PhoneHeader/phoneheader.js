import { Component } from "react";
import '../header.css';
import { Navbar, Container, Nav } from "react-bootstrap";
import { Badge } from "react-bootstrap";

import * as React from 'react';
import AppBar from '@mui/material/AppBar';

// import Avatar from '@mui/material/Avatar';
// import image from "./nic.png";

const FULLNAME = process.env.REACT_APP_FULLNAME;

class PhoneHeader extends Component {

    render() {
      return (
        <div className="">
          <React.Fragment>
              <AppBar position="fixed" color="primary" sx={{ top: 'auto', bottom: 0 }}>
                  <Navbar id="phoneheader" collapseOnSelect expand="lg" variant="dark">
                    <Container>
                      <Navbar.Brand href="#contact">{FULLNAME}</Navbar.Brand>
                      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                      <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto">
                          <Nav.Link href="#contact">Contacto</Nav.Link>
                          <Nav.Link href="#education">Educación</Nav.Link>
                          <Nav.Link href="#experience">Experiencia</Nav.Link>
                          <Nav.Link href="#portfolio">Portafolio</Nav.Link>
                          <Nav.Link href="#courses">Cursos</Nav.Link>
                          <Nav.Link href="#additional">Conocimientos</Nav.Link>
                          <Nav.Link href="#">Descargar Pdf <Badge bg="primary" className="btn-outline-light" style={{marginLeft: '11px'}}>En Desarrollo</Badge> </Nav.Link>
                          
                            {/* <NavDropdown title="Descargar CV" id="collasible-nav-dropdown">
                                <NavDropdown.Item href='/CVNicolasGrillo.pdf' download>Documento PDF <Badge bg="primary" className="btn-outline-light" style={{marginLeft: '11px'}}>En Desarrollo</Badge> </NavDropdown.Item>
                                <NavDropdown.Item href='./CVNicolasGrillo.docx' download>Microsoft Word  </NavDropdown.Item>
                            </NavDropdown> */}
                          
                        </Nav>
                      </Navbar.Collapse>
                    </Container>
                  </Navbar>
              </AppBar>
          </React.Fragment>
        </div>
      );
    }
  }
  
  export default PhoneHeader;