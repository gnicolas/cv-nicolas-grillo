
import './Menu.css';

import {motion} from 'framer-motion/dist/framer-motion';
import { useCycle } from 'framer-motion/dist/framer-motion';

import Button from '@mui/material/Button';

import Avatar from '@mui/material/Avatar';
import image from "../Menu/nicolas.jpg";
const imgSize = "222px";

const sidebar = {
    open: (height = 1000) => ({
      clipPath: `circle(${height * 2 + 200}px at 40px 40px)`,
      transition: {
        type: "spring",
        stiffness: 20,
        restDelta: 2
      }
    }),
    closed: {
      clipPath: "circle(32px at 40px 40px)",
      transition: {
        delay: 0.5,
        type: "spring",
        stiffness: 400,
        damping: 40
      }
    }
  };

  const subSidebar = {
    open: (height = 1000) => ({
      clipPath: `circle(${height * 2 + 200}px at 40px 40px)`,
      transition: {
        type: "spring",
        stiffness: 20,
        restDelta: 2
      }
    }),
    closed: {
      clipPath: "circle(1px at 1px 1px)",
      transition: {
        delay: 0.5,
        type: "spring",
        stiffness: 400,
        damping: 40
      }
    }
  };

const variantsNavigation = {
    open: {
        transition: { staggerChildren: 0.07, delayChildren: 0.2 }
    },
    closed: {
        transition: { staggerChildren: 0.05, staggerDirection: -1 }
    }
};

const variantsItem = {
    open: {
        y: 0,
        opacity: 1,
        transition: {
        y: { stiffness: 1000, velocity: -100 }
        }
    },
    closed: {
        y: 50,
        opacity: 0,
        transition: {
        y: { stiffness: 1000 }
        }
    }
};

function createData(id, section, ref) {
    return { id, section, ref};
}

const menus = [
    createData(0, 'Contacto', '#contact'),
    createData(1, 'Educacion', '#education'),
    createData(2, 'Experiencia', '#experience'),
    createData(3, 'Portafolio', '#portfolio'),
    createData(4, 'Cursos', '#courses')
];

const Path = props => (
    <motion.path 
      fill="none"
      strokeWidth="4"
      stroke="#fff"
      strokeLinecap="round"
      {...props}
    />
  );

function Menu(cc) {

    const [isOpen, toggleOpen] = useCycle(false, true);

    return (
        <div className="">

            <header id="header">

                
                <motion.nav className="menu-nav container justify-content-center"
                    initial={true}
                    animate={isOpen ? "closed" : "open"}
                    custom={{ width: 100, height: 100 }}
                    // ref={containerRef}
                >
                    <motion.div className="background" variants={sidebar} />

                    <motion.ul variants={variantsNavigation}>

                        <motion.li variants={variantsItem} >
                            <Button href="#contact" style={{width: '100%', marginTop: '69px', marginBottom: '22px'}} variant="text">
                                <Avatar className="center" style={{ width: imgSize, height: imgSize}} alt="x" src={image} />
                            </Button>
                        </motion.li>
                    
                        {menus.map(menu => (
                            <motion.li id={menu.id} variants={variantsItem} whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.95 }} >
                                <Button href={menu.ref} className="effect" style={{backgroundColor: `var(--color-header-opacity)`, color: 'white', fontWeight: 'bold', width: '100%', marginTop: '5px', marginBottom: '11px'}} variant="contained">
                                    {menu.section}
                                </Button>
                            </motion.li>
                        ))}
                    </motion.ul>


                    <button class="menu-button" onClick={() => toggleOpen()}>
                        <svg className='svg-shadow ' width="200" height="200" viewBox="0 0 113 111">
                        <Path
                            variants={{
                            closed: { d: "M 2 2.5 L 20 2.5" },
                            open: { d: "M 3 16.5 L 17 2.5" }
                            }}
                        />
                        <Path 
                            d="M 2 9.423 L 20 9.423"
                            variants={{
                            closed: { opacity: 1 },
                            open: { opacity: 0 }
                            }}
                            transition={{ duration: 0.1 }}
                        />
                        <Path 
                            variants={{
                            closed: { d: "M 2 16.346 L 20 16.346" },
                            open: { d: "M 3 2.5 L 17 16.346" }
                            }}
                        />
                        </svg>
                    </button>

                </motion.nav>
            </header>

            <div id="subheader"style={{backgroundColor: 'var(--color-header)', backgroundImage: 'var(--color-header-lineal)'}} >
                <motion.nav className="menu-nav" initial={true} animate={isOpen ? "closed" : "open"} custom={{ width: 100, height: 100 }} >

                    <motion.div className="background" variants={subSidebar} />

                </motion.nav>
            </div>

        </div>
    );
}
  
export default Menu;