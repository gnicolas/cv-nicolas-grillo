
const YEAR = 365;
const MONTH = 30;

export function getTimer(time) {
    let totalDays = Math.ceil(time / (1000 * 3600 * 24));

    let years = 0;
    let months = 0;

    do {
        if(totalDays >= YEAR) {
            years ++;
            totalDays -= YEAR;
        } else {
            if(totalDays >= MONTH) {
                months ++;
                totalDays -= MONTH;
            }
        }
    } while (totalDays >= MONTH);

    let result = "";

    if(years !== 0) {
        result += years.toString();
        if (years > 1) {
            result += " años";
        } else {
            result += " año";
        }
    }

    if(months !== 0) {

        if(years !== 0) {
            result += ", ";
        }

        result += months.toString();
        if (months > 1) {
            result += " meses";
        } else {
            result += " mes";
        }
    } 
    
    return result;
}