import './App.css';
import './header/header.css'
import Button from '@mui/material/Button';
import DocuPdf from "./main/docuPdf";
import { PDFViewer } from "@react-pdf/renderer";
import Main from './main/main';
import { Component } from 'react';
import PhoneHeader from './header/PhoneHeader/phoneheader';

// import { PDFDownloadLink } from "@react-pdf/renderer";

import Menu from './header/Menu/Menu';

const bgBtnHeader = 'var(--color-btn-head)';

const svgHome =
<svg width="25" height="25" viewBox="0 0 24 24">
    <path fill="currentColor" d="M10,20V14H14V20H19V12H22L12,3L2,12H5V20H10Z" />
</svg>;

const svgEye =
<svg width="25" height="25" viewBox="0 0 24 24">
    <path fill="currentColor" d="M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9M12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17M12,4.5C7,4.5 2.73,7.61 1,12C2.73,16.39 7,19.5 12,19.5C17,19.5 21.27,16.39 23,12C21.27,7.61 17,4.5 12,4.5Z" />
</svg>;

const svgPdf =
<svg width="25" height="25" viewBox="0 0 24 24">

<path fill="currentColor" d="M17,13L12,18L7,13H10V9H14V13M19.35,10.03C18.67,6.59 15.64,4 12,4C9.11,4 6.6,5.64 5.35,8.03C2.34,8.36 0,10.9 0,14A6,6 0 0,0 6,20H19A5,5 0 0,0 24,15C24,12.36 21.95,10.22 19.35,10.03Z" />

</svg>;

class App extends Component{

  state = {
    disabled: true,
    text: svgEye
  }
    
  updateState = () => {
    let text = !this.state.disabled ? svgEye : svgHome;
    this.setState({ disabled: !this.state.disabled, text: text })
  }

  render() {
    return (
      <div className="d-flex">

        { this.state.disabled ? 
            <Menu></Menu> : null 
        }

        <PhoneHeader/>

        { !this.state.disabled ?
          <div id="pdf-header">
            <ul className="navbar-nav">
              <li>
                <Button onClick={this.updateState} style={{backgroundColor: bgBtnHeader, width: '100%', marginTop: '5px', marginBottom: '11px'}} variant="contained">
                  {this.state.text}
                </Button>
              </li>
              <li>
                <Button style={{backgroundColor: bgBtnHeader, width: '100%', marginTop: '5px', marginBottom: '11px'}} variant="contained">
                  {svgPdf}
                </Button>
              </li>
            </ul>
          </div> : null
        }

        { this.state.disabled ? 
          <Main/> : null 
        }

        { 
          !this.state.disabled ? 
          <PDFViewer id="docu-pdf" style={{marginLeft: 'var(--widht-subheader)'}}>
            <DocuPdf></DocuPdf>
          </PDFViewer> : null 
        }        

      </div>
    );
  }
}

export default App;